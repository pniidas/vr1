<?php 
	if(isset($_POST['submit']) && isset($_POST['action']) && ($_POST['action'] == "additem" || $_POST['action'] == "edititem")){
		
		$itemname = NULL;

		if(isset($_POST['itemname']) && $_POST['itemname'] !=""){
			$itemname = $_POST['itemname'];
		}else{
			$errors['itemname'] = "Item name missing";
		}


		$itemdescription = NULL;

		if(isset($_POST['itemdescription']) && $_POST['itemdescription'] !=""){
			$itemdescription = $_POST['itemdescription'];
		}else{
			$errors['itemdescription'] = "Item description missing";
		}

		$itemquantity = NULL;

		if(isset($_POST['itemquantity']) && ctype_digit($_POST['itemquantity'])){
			$itemquantity = $_POST['itemquantity'];
		}else{
			$errors['itemquantity'] = "Item quantity missing or not a number";
		}

		if(count($errors) == 0){
			if($_POST['action'] == "additem"){
				mysqli_query($connect,"INSERT INTO pniidas_items (userid, name, description, quantity) VALUES (".$_SESSION['user']['id'].",'".$itemname."', '".$itemdescription."', ".$itemquantity.")");
			}elseif($_POST['action'] == "edititem"){
				mysqli_query($connect, "UPDATE pniidas_items SET name = '".$itemname."', description = '".$itemdescription."', quantity = ".$itemquantity." WHERE id = ".$_POST['itemid']);
			}
			
		}

	}

	if(isset($_GET['action']) && $_GET['action'] == "delete" && isset($_GET['item'])){
		mysqli_query($connect, "DELETE FROM pniidas_items WHERE id = ". $_GET['item']);
	}

	$isediting = FALSE;
	$edititemid = 0;
	$edititem = NULL;

	if(isset($_GET['action']) && $_GET['action'] == "edit" && isset($_GET['item'])){
		$isediting = TRUE;
		$edititemid = $_GET['item'];
	}


	$fetchresult = mysqli_query($connect, "SELECT * FROM pniidas_items WHERE userid = ".$_SESSION['user']['id']);

	$rows = array();

	while($row = mysqli_fetch_assoc($fetchresult)){
		$rows[] = $row;

		if($isediting && $row['id'] == $edititemid){
			$edititem = $row;
		}
	}

?>

<div class="row">
	<div class="col-lg-offset-2 col-lg-8" style="background-color:#efefef; padding:20px;">
		<h1>Items</h1>
		<form class="form-horizontal" style="margin:20px;" method="POST" action="<?php echo $_SERVER['PHP_SELF'];?>/?page=warehouse">
			<div class="form-group">
				<label class="control-label">Item name </label>
				<input type="text" name="itemname" class="form-control" <?php if($isediting){ echo 'value = "'.$edititem['name'].'"';}?>/>
					<?php 
					if(isset ($errors['itemname'])){
						echo '<span class="help-block">'.$errors['itemname'].'</span>';
					}
				?>
			</div>
			<div class="form-group">
				<label class="control-label">Item description </label>
				<input type="text" name="itemdescription" class="form-control" <?php if($isediting){ echo 'value = "'.$edititem['description'].'"';}?>/>
					<?php 
					if(isset ($errors['itemdescription'])){
						echo '<span class="help-block">'.$errors['itemdescription'].'</span>';
					}
				?>
			</div>
			<div class="form-group">
				<label class="control-label">Item quantity </label>
				<input type="text" name="itemquantity" class="form-control"  <?php if($isediting){ echo 'value = "'.$edititem['quantity'].'"';}?>/>
					<?php 
					if(isset ($errors['itemquantity'])){
						echo '<span class="help-block">'.$errors['itemquantity'].'</span>';
					}
				?>
			</div>


			<div class="form-group">
				<input type="hidden" name="itemid" <?php if($isediting){ echo 'value = "'.$edititem['id'].'"';}?>/>
				<input type="hidden" name="action" <?php if($isediting){ echo 'value = "edititem"';}else{ echo 'value = "additem"';}?>/>
				<input type="submit" name="submit" class="form-control"  <?php if($isediting){ echo 'value = "Edit item"';}else{ echo 'value = "Add item"';}?>/>
			</div>
		</form>
		<table class="table ta">
			<thead>
				<tr>
					<th>Name</th>
					<th>Description</th>
					<th>Quantity</th>
					<th></th>
				</tr>
			</thead>
			<tbody>
			<?php 
				for($i = 0; $i < count($rows);$i++){

				
			?>
				<tr>
					<td><?php echo $rows[$i]['name'];?></td>
					<td><?php echo $rows[$i]['description'];?></td>
					<td><?php echo $rows[$i]['quantity'];?></td>
					<td><a href="<?php echo $_SERVER['PHP_SELF'];?>/?page=warehouse&action=edit&item=<?php echo $rows[$i]['id'];?>">Edit</a> | <a href="<?php echo $_SERVER['PHP_SELF'];?>/?page=warehouse&action=delete&item=<?php echo $rows[$i]['id'];?>">Delete</a></td>

				</tr>
				<?php  
					}
				?>
			</tbody>
		</table>
		
	</div>
</div>
