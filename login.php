<div class="row">
	<div class="col-lg-offset-4 col-lg-4" style="background-color:#efefef; padding:20px;">
		<h1> Login </h1>
		<form class="form-horizontal" style="margin:20px;" method="POST" action="<?php echo $_SERVER['PHP_SELF'];?>/?page=login">
			<div class="form-group">
				<label class="control-label">E-Mail </label>
				<input type="text" name="email" class="form-control"/>
			</div>
			<div class="form-group">
				<label class="control-label">Password </label>
				<input type="password" name="password" class="form-control"/>
			</div>
			<div class="form-group">
				<input type="hidden" name="action" value="login"/>
				<input type="submit" name="submit" class="form-control" value="Log in"/>
			</div>
		</form>
		<p class="text-center"><a href="<?php echo $_SERVER['PHP_SELF']; ?>/?page=register">Register</a></p>
	</div>
</div>

