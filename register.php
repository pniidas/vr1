<?php
	var_dump($_POST);

	$errors = array();


	if(isset($_POST['submit']) ){
		
		$firstname = NULL;

		if(isset($_POST['firstname']) && $_POST['firstname'] !=""){
			$firstname = $_POST['firstname'];
		}else{
			$errors['firstname'] = "First name missing";
		}


		$lastname = NULL;

		if(isset($_POST['lastname']) && $_POST['lastname'] !=""){
			$lastname = $_POST['lastname'];
		}else{
			$errors['lastname'] = "Lastname missing";
		}

		$password = NULL;

		if(isset($_POST['password']) && $_POST['password'] !=""){
			$password = $_POST['password'];
		}else{
			$errors['password'] = "Password missing";
		}

		$passwordagain = NULL;

		if(isset($_POST['passwordagain']) && $_POST['passwordagain'] !=""){
			$passwordagain = $_POST['passwordagain'];
		}else{
			$errors['passwordagain'] = "Second password missing";
		}

		if($password != $passwordagain){
			$errors['passwordagain'] = "Second password incorrect";
		}

		$email = NULL;

		if(isset($_POST['email'])){
			$email = $_POST['email'];
			if(!filter_var($email, FILTER_VALIDATE_EMAIL)){
				$errors['email'] = "Email is non valid"; 
			}
		}else{
			$errors['email'] = "Email missing";
		}

		if(count($errors) == 0){

			$result = 	 mysqli_query($connect, "INSERT INTO pniidas_users (firstname, lastname, password, email) VALUES ('".$firstname."', '".$lastname."','".$password."','".$email."')");
			if(!$result && mysqli_errno($connect) == 1062){
				$errors['email'] = "Email is in use tard!";
			}


		}

	}

	
?>

<div class="row">
	<div class="col-lg-offset-4 col-lg-4" style="background-color:#efefef; padding:20px;">
		<h1> Register </h1>
		<form class="form-horizontal" style="margin:20px;" method="POST" action="<?php echo $_SERVER['PHP_SELF'];?>/?page=register">
			<div class="form-group">
				<label class="control-label">First Name </label>
				<input type="text" name="firstname" class="form-control"/>
				<?php 
					if(isset ($errors['firstname'])){
						echo '<span class="help-block">'.$errors['firstname'].'</span>';
					}
				?>
			</div>
			<div class="form-group">
				<label class="control-label">Last Name </label>
				<input type="text" name="lastname" class="form-control"/>
				<?php 
					if(isset ($errors['lastname'])){
						echo '<span class="help-block">'.$errors['lastname'].'</span>';
					}
				?>
			</div>
			<div class="form-group">
				<label class="control-label">E-Mail </label>
				<input type="text" name="email" class="form-control"/>
				<?php 
					if(isset ($errors['email'])){
						echo '<span class="help-block">'.$errors['email'].'</span>';
					}
				?>
			</div>
			<div class="form-group">
				<label class="control-label">Password </label>
				<input type="password" name="password" class="form-control"/>
				<?php 
					if(isset ($errors['password'])){
						echo '<span class="help-block">'.$errors['password'].'</span>';
					}
				?>
			</div>
			<div class="form-group">
				<label class="control-label">Password Again </label>
				<input type="password" name="passwordagain" class="form-control"/>
				<?php 
					if(isset ($errors['passwordagain'])){
						echo '<span class="help-block">'.$errors['passwordagain'].'</span>';
					}
				?>
			</div>
			<div class="form-group">
				<input type="hidden" name="action" value="register"/>
 				<input type="submit" name="submit" class="form-control" value="Register"/>
			</div>
		</form>
	</div>
</div>
