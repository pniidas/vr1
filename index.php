<?php 
session_start(); 

include 'mysqli.php';

$errors = array();
$pageIndex= "http://localhost/projectvr";
$page = "login.php";
$redirectpage = FALSE;

if(isset($_POST['submit']) && isset($_POST['action']) && $_POST['action'] == "login"){

	$email = NULL;

	if(isset($_POST['email'])){
		$email = $_POST['email'];
		if(!filter_var($email, FILTER_VALIDATE_EMAIL)){
			$errors['email'] = "Email incorrect!"; 
		}
	}

	$password = NULL;


	if(isset($_POST['password'])){
		$password = $_POST['password'];
	}else{
		$errors['password'] = "Password incorrect!";
	}

	if(count($errors) == 0){
		$result = mysqli_query($connect, "SELECT * FROM pniidas_users WHERE email = '".$email."' LIMIT 1");

		$user = mysqli_fetch_assoc($result);

		if($user['password'] == $password){
			$_SESSION['user'] = $user;
			$page = "warehouse.php";
			$redirectpage = TRUE;
		}

		mysqli_free_result($result);
	}
}

if (isset($_GET["page"]) && !$redirectpage)
{
	$pageName = $_GET["page"]; 

	if($pageName == "login" || $pageName == "register" || $pageName == "warehouse")
	{
		$page = $pageName . ".php";
	}

}


?>

<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
   
    <meta name="description" content="">
    <meta name="author" content="">
  

    <title>Project VR1</title>

   
    <link href="<?php echo $pageIndex;?>/styles/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <link href="<?php echo $pageIndex;?>/styles/main.css" rel="stylesheet">

  </head>

  <body>

    <nav class="navbar navbar-inverse navbar-fixed-top">
      <div class="container">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="#">Project VR1</a>
        </div>
       
      </div>
    </nav>

    <div class="container">

      <div class="content">
       <?php 

       include $page;

       ?>
      </div>

    </div>


   
 
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    <script>window.jQuery || document.write('<script src="../../assets/js/vendor/jquery.min.js"><\/script>')</script>
    <script src="<?php echo $pageIndex;?>/styles/bootstrap/js/bootstrap.min.js"></script>
    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
    <script src="../../assets/js/ie10-viewport-bug-workaround.js"></script>
  </body>
</html>
